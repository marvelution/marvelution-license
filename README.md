This project features a license resource library used with the maven-license-plugin plugin

Continuous Builder
==================
<https://marvelution.atlassian.net/builds/browse/MARVADMIN-LIC>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
